# Example CBT_V2 + Laradock + PostgreSQL + PostGIS
Run CBT_V2 on latest Laradock with PostgreSQL + PostGIS.

Based on https://laradock.io/getting-started/#b-setup-for-multiple-projects.

## Requirements
* Docker and Docker Compose installed
  * https://docs.docker.com/engine/install/
* for Linux: the user must belong the docker group to allow running `docker` without `sudo` privilege
  * `sudo usermod -aG docker ${USER}`
* Port 80 + 443 available for Nginx container.
* Port 5432 available for PostgreSQL container.
* Port 6379 available for Redis container.
* Port 8125 available for Mailpit container.

## How to run
SSH to the target server and run:
```
cd /path/to/working/dir

git clone --recursive https://gitlab.com/senoadiw/cbt_v2.git

cd cbt_v2

# use default laravel .env file
cp ./cbt_v2/.env.example ./cbt_v2/.env

cd ./cbt_v2/laradock

# modify example laradock .env
cp .env.example .env

nano .env
  # add to bottom
  DATA_PATH_HOST=~/.laradock/data
  COMPOSE_PROJECT_NAME=cbt_v2
  POSTGRES_DB=cbt_database
  POSTGRES_USER=cbt_user
  POSTGRES_PASSWORD=cbtuserpassword
  LARAVEL_HORIZON_INSTALL_PHPREDIS=true
  PHP_FPM_INSTALL_PGSQL=true
  PHP_WORKER_INSTALL_PGSQL=true

# configure php-worker container
cd php-worker/supervisord.d
cp laravel-worker.conf.example laravel-worker.conf
nano laravel-worker.conf
  command=php /var/www/cbt_v2/artisan queue:work --sleep=3 --tries=3
  numprocs=2
cp laravel-scheduler.conf.example laravel-scheduler.conf
nano laravel-scheduler.conf
  command=/bin/sh -c "while [ true ]; do (php /var/www/cbt_v2/artisan schedule:run --verbose --no-interaction &); sleep 60; done"
cd ../..

# configure laravel horizon container
cd laravel-horizon/supervisor.d
cp laravel-horizon.conf.example laravel-horizon.conf
nano laravel-horizon.conf
  command=php /var/www/cbt_v2/artisan horizon
  stdout_logfile=/var/www/cbt_v2/storage/logs/horizon.log
cd ../..

# bring up the containers
docker compose up -d nginx postgres-postgis redis mailpit laravel-horizon workspace

# check container status
docker compose ps

# check the logs and wait a moment for the containers to initialize
docker compose logs -f
docker compose logs workspace -f

# install composer packages and run initial database migration
docker compose exec -it --user=laradock workspace bash
  cd cbt_v2
  composer install
  php artisan migrate

# add local domain to hosts file (/etc/hosts)
  127.0.0.1 cbt_v2.test

# access
  http://cbt_v2.test
  http://cbt_v2.test/login
  http://cbt_v2.test/register
  http://cbt_v2.test/horizon

# done!
```

## Useful commands
```
# to stop the containers
docker compose stop

# to start the containers
docker compose up -d nginx postgres-postgis redis mailpit laravel-horizon workspace

# run laravel artisan
docker compose exec -it --user=laradock workspace bash
  cd cbt_v2
  php artisan
  exit

# to delete the containers
docker compose down

# to delete the containers and volumes (warning: this will delete ALL databases)
docker compose down -v
```

## Restore 20240604_cbt_database.backup
```
# check database and enable postgis extension
docker compose exec -it postgres-postgis psql -U cbt_user cbt_database
  \dt
  \dx
  CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology; CREATE EXTENSION pg_trgm;
  \dx
  \q

# copy backup to container
# check postgres-postgis container name
docker compose ps
docker cp /path/to/20240604_cbt_database.backup cbt_v2-postgres-postgis-1:/tmp

# pg_restore backup to postgres-postgis container
docker compose exec -it postgres-postgis pg_restore --host localhost --port 5432 --username "cbt_user" --dbname "cbt_database" --password --schema-only --verbose /tmp/20240604_cbt_database.backup
  cbtuserpassword
docker compose exec -it postgres-postgis pg_restore --host localhost --port 5432 --username "cbt_user" --dbname "cbt_database" --password --data-only --disable-triggers --verbose /tmp/20240604_cbt_database.backup
  cbtuserpassword

# check restored tables
docker compose exec -it postgres-postgis psql -U cbt_user cbt_database
  \dt

# create initial backup
docker compose exec -it postgres-postgis pg_dump --host localhost --port 5432 --username "cbt_user" --format custom --blobs --file /tmp/cbt_database.laravel.backup cbt_database
  cbtuserpassword

# copy backup to current dir
docker cp cbt_v2-postgres-postgis-1:/tmp/cbt_database.laravel.backup ./

# move backup outside repo
mv ./cbt_database.laravel.backup ~/

# delete backup files in container
docker compose exec -it postgres-postgis bash
  ls /tmp
  rm /tmp/*.backup
  exit
```

# restore initial backup
```
docker compose exec -it postgres-postgis psql -U cbt_user postgres -c "DROP DATABASE cbt_database;"
docker compose exec -it postgres-postgis psql -U cbt_user cbt_database -c "CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology; CREATE EXTENSION pg_trgm;"
docker cp /path/to/cbt_database.laravel.backup cbt_v2-postgres-postgis-1:/tmp
docker compose exec -it postgres-postgis pg_restore --host localhost --port 5432 --username "cbt_user" --dbname "cbt_database" --password --schema-only --verbose /tmp/cbt_database.laravel.backup
  cbtuserpassword
docker compose exec -it postgres-postgis pg_restore --host localhost --port 5432 --username "cbt_user" --dbname "cbt_database" --password --data-only --disable-triggers --verbose /tmp/cbt_database.laravel.backup
  cbtuserpassword
docker compose exec -it postgres-postgis bash -c "rm /tmp/*.backup"
```

## Connecting to the database
Using tools such as DBeaver, it is possible to connect to the database from the Docker host. Refer to the database credentials in the `cbt_v2/.env` file and set the host to `localhost` and port to `5432`.

DBeaver connection settings:
* Host: localhost
* Port: 5432
* Database: refer to the `.env` file
* Username: refer to the `.env` file
* Password: refer to the `.env` file
